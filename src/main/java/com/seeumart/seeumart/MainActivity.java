package com.seeumart.seeumart;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    String ShowOrHideWebViewInitialUse = "show";
    Boolean isPageLoadedComplete = false;
    private WebView webview;
    private ProgressBar spinner;
//    private ProgressBar spinner;
//    private TextView titleLabel;
    private ImageView logoImage;
//    private TextView errorMessage;
//    private Button retryButton;
    private SwipeRefreshLayout swipeRefreshLayout;

//    private String lastVisitedUrl = "http://wap.seeumart.my";

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webview = (WebView) findViewById(R.id.webview);
//        titleLabel = (TextView) findViewById(R.id.titleLabel);
        logoImage = (ImageView) findViewById(R.id.logo_image);
        spinner = (ProgressBar) findViewById(R.id.progressBar1);
//        spinner = (ProgressBar) findViewById(R.id.progressBar2);
//        errorMessage = (TextView) findViewById(R.id.error_message);
//        retryButton = (Button) findViewById(R.id.retry_button);
        webview.setWebViewClient(new CustomWebViewClient());
        swipeRefreshLayout = (SwipeRefreshLayout) this.findViewById(R.id.swipeContainer);

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setUserAgentString("Android");
        webview.setWebChromeClient(new WebChromeClient());
        webview.setOverScrollMode(WebView.OVER_SCROLL_NEVER);

//        retryButton.setVisibility(View.INVISIBLE);
//        errorMessage.setVisibility(View.INVISIBLE);

//        retryButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                spinner.setVisibility(View.VISIBLE);
//                spinner.bringToFront();
//                webview.reload();
//            }
//        });

        if (Build.VERSION.SDK_INT >= 19) {
            webview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

//        Timer myTimer = new Timer();
//
//        myTimer.schedule(new loaderTask(), 30000);

        webview.loadUrl("http://wap.seeumart.my");
        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (isOnline()) {
                            webview.reload();
                        } else {
                            showDialog("The internet is not available", "Please try again");
                        }
                    }
                }
        );

//        BottomNavigationView bottomNavigationView = (BottomNavigationView)
//                findViewById(R.id.bottom_navigation);
//
//        bottomNavigationView.setOnNavigationItemSelectedListener(
//                new BottomNavigationView.OnNavigationItemSelectedListener() {
//                    @Override
//                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                        switch (item.getItemId()) {
//                            case R.id.home:
//                                Toast.makeText(MainActivity.this, "haha", Toast.LENGTH_SHORT).show();
//                            case R.id.back:
//
//                            case R.id.forward:
//
//                            case R.id.refresh:
//
//                        }
//                        return true;
//                    }
//                });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
            webview.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void showDialog(String title, String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
        builder1.setTitle(title);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        webview.reload();

                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "Exit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

        if (webview.getVisibility() != View.INVISIBLE) {
            webview.setVisibility(View.INVISIBLE);
        }
    }

    private class CustomWebViewClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (isOnline()) {
//                if (url.contains("wap.mix.com.my")) {
//                    if (webview.getVisibility() == View.INVISIBLE) {
//                        webview.setVisibility(View.VISIBLE);
////                        retryButton.setVisibility(View.INVISIBLE);
////                        errorMessage.setVisibility(View.INVISIBLE);
//                    }
//
//                    spinner.setVisibility(View.VISIBLE);
//                    spinner.bringToFront();
//
//                    return false;
//                } else {
//                    view.getContext().startActivity(
//                            new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
//
//                    return true;
//                }

                if (url.contains("facebook.com") || url.contains("twitter.com") || url.contains("instagram.com")) {
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

                    return true;
                } else {
                    spinner.setVisibility(View.VISIBLE);
                    spinner.bringToFront();

                    return false;
                }
            } else {
//                if (webview.getVisibility() != View.INVISIBLE) {
//                    webview.setVisibility(View.INVISIBLE);
//                }

                showDialog("The internet is not available", "Please try again");

                return true;
            }
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest req) {
            if (isOnline()) {
//                if (req.getUrl().toString().contains("wap.mix.com.my")) {
//                    if (webview.getVisibility() == View.INVISIBLE) {
//                        webview.setVisibility(View.VISIBLE);
////                        retryButton.setVisibility(View.INVISIBLE);
////                        errorMessage.setVisibility(View.INVISIBLE);
//                    }
//
//                    spinner.setVisibility(View.VISIBLE);
//                    spinner.bringToFront();
//
//                    return false;
//                } else {
//                    view.getContext().startActivity(
//                            new Intent(Intent.ACTION_VIEW, req.getUrl()));
//
//                    return true;
//                }

                if (req.getUrl().toString().contains("facebook.com") || req.getUrl().toString().contains("twitter.com") || req.getUrl().toString().contains("instagram.com")) {
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, req.getUrl()));

                    return true;
                } else {
                    spinner.setVisibility(View.VISIBLE);
                    spinner.bringToFront();

                    return false;
                }

            } else {
                showDialog("The internet is not available", "Please try again");

                return true;
            }
        }

//        @Override
//        public void onPageStarted(WebView webview, String url, Bitmap favicon) {
//            if (ShowOrHideWebViewInitialUse.equals("show")) {
//                webview.setVisibility(webview.INVISIBLE);
//            }
//        }


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            spinner.setVisibility(View.VISIBLE);
            spinner.bringToFront();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            swipeRefreshLayout.setEnabled(true);

            logoImage.setVisibility(View.INVISIBLE);
            spinner.setVisibility(View.INVISIBLE);

            if (isOnline()) {
                webview.setVisibility(View.VISIBLE);
            }

            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
        }

        // < Android 5
        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            //Handle the error
            super.onReceivedError(view, errorCode, description, failingUrl);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

                String message = null;
                String title = null;

                if (isOnline()) {
                    if (errorCode == WebViewClient.ERROR_TIMEOUT) {
                        message = "The server is taking too much time to communicate. Try again later.";
                        title = "Connection Timeout";
                    }
//                else if (errorCode == WebViewClient.ERROR_HOST_LOOKUP) {
//                    message = "Server or proxy hostname lookup failed";
//                    title = "Host Lookup Error";
//                }

                    if (message != null) {
                        showDialog(title, message);
                    }

                    Toast.makeText(MainActivity.this, "Timeout 1", Toast.LENGTH_SHORT).show();
                } else {
                    showDialog("The internet is not available", "Please try again");
                }
            }
        }

        // > Android 5
        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);

            String message = null;
            String title = null;

            if (isOnline()) {
                if (error.getErrorCode() == WebViewClient.ERROR_TIMEOUT) {
                    message = "The server is taking too much time to communicate. Try again later.";
                    title = "Connection Timeout";
                }
                //            else if (error.getErrorCode() == WebViewClient.ERROR_HOST_LOOKUP) {
                //                message = "Server or proxy hostname lookup failed";
                //                title = "Host Lookup Error";
                //            }

                if (message != null) {
                    showDialog(title, message);
                }

                System.out.println(error.getErrorCode());

                //            Toast.makeText(MainActivity.this, "Timeout 2", Toast.LENGTH_SHORT).show();
            } else {
                showDialog("The internet is not available", "Please try again");
            }
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
        }
    }
}

